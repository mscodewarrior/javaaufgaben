package stringbuilder;

public class SHTest {

    public static void main(String[] args) {

        StringHelper myHelper = new StringHelper();

        System.out.println(myHelper.searchAndReplace("Hallo wie geht es dir?", "dir", "Ihnen"));
        System.out.println(myHelper.searchAndReplace("A word is still a word", "word", "car"));
        System.out.println(myHelper.searchAndReplace("A word is still a word", "world", "car"));
    }

}