package stringbuilder;

public class StringHelper {

    public String searchAndReplace(String in, String search, String replace) {
        StringBuilder myBuilder = new StringBuilder(in);
        int startIndex = 0;
        int laengeSearch = search.length();
        while (startIndex != -1) {
            startIndex = myBuilder.indexOf(search, startIndex);
            if (startIndex != -1) {
                myBuilder.replace(startIndex, startIndex + laengeSearch, replace);
            }
        }
        return myBuilder.toString();
    }

}