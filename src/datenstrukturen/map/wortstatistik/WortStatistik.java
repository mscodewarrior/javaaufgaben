package datenstrukturen.map.wortstatistik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Set;

/**
 * Diese Klasse dient dazu die Verwendung einer HashMap zu demonstrieren.
 * 
 * Dieser Code ist Public Domain und kann von jedem nach belieben genutzt werden.
 * 
 * @author Marcus Schmidt
 */
public class WortStatistik {

	public static void main(String[] args) throws IOException {
	
		// Map als Hilfsmittel erzeugen
		HashMap<String, Integer> statMap = new HashMap<String, Integer>();
		
		// Text eingeben
		System.out.println("Bitte Text eingeben (nur Worte, Leerzeichen und Punkt):");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String eingabe = reader.readLine();
		eingabe = eingabe.toUpperCase(); // Gross-/Kleinschreibung ist nicht relevant

		// Statistik erzeugen
		
		// Text in Worte aufsplitten
		String[] worte = eingabe.split(" ");
		
		// Ueber alle Worte iterieren
		for (String wort: worte) {
			if (!".".equals(wort)) { // Punkte werden nicht beachtet
				if (statMap.get(wort) == null) {
					// das Wort ist noch nicht in der Statistik 
					statMap.put(wort, 1);
				} else {
					// das Wort gibt es schon - wir muessen hochzaehlen
					Integer anzahl = statMap.get(wort);
					anzahl++; // autounboxing!
					statMap.put(wort, anzahl);
				}	
			}
		}
		
		// Statistik ausgeben
		System.out.println("Es wurden " + statMap.size() + " unterschiedliche Worte verwendet.");
		Set<String> keySet = statMap.keySet(); // wir brauchen zuerst ein Set mit allen Keys
		// ueber alle Schluessel iterieren und Wort und Anzahl ausgeben
		for (String key: keySet) {
			Integer anzahl = statMap.get(key);
			System.out.println(key + " -> " + anzahl);
		}
		
	}

}
