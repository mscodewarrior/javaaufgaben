package arrays.temperaturdaten;

public class Temperaturdaten {

	public static void main(String[] args) {
		int[] temperatur = { 20, 17, 16, 10, 9, 10, 19, 22, 24, 23, 24, 25, 26, 22 };

		System.out.print("Tag:        |");
		for (int i = 1; i <= temperatur.length; i++) {
			String dayAsString = String.valueOf(i);
			if (dayAsString.length() < 2) {
				dayAsString = " " + dayAsString;
			}
			System.out.print(dayAsString + "|");
		}
		System.out.print("\nTemperatur: |");

		int min = temperatur[0];
		int max = temperatur[0];
		int summe = 0;
		
		for (int i = 0; i < temperatur.length; i++) {
			String tempAsString = String.valueOf(temperatur[i]);
			if (tempAsString.length() < 2) {
				tempAsString = " " + tempAsString;
			}
			System.out.print(tempAsString + "|");
			if (min > temperatur[i]) {
				min = temperatur[i];
			}
			if (max < temperatur[i]) {
				max = temperatur[i];
			}
			summe += temperatur[i];
		}

	    int durchschnitt = summe / temperatur.length;
		System.out.println("\nDurchschnitt: " + durchschnitt);
		System.out.println("Maximum: " + max);
		System.out.println("Minimum: " + min);
		
	}

}
