import java.util.Random;
import java.util.Scanner;

public class Minenfeld {

	// Minenfeld komplett ausgeben
	public static void printFullMineField(int[][][] spielfeld) {
		System.out.println(" X01234567");
		System.out.println("Y#########");
		for (int y = 0; y < 8; y++) {
			System.out.print(y+"#");
			for (int x = 0; x < 8; x++) {
				if (spielfeld[y][x][0] != -1) {
					System.out.print(spielfeld[y][x][0]);
				} else {
					System.out.print('M');
				}
			}
			System.out.println("");
		}
	}

	// Minenfeld ausgeben, soweit es schon aufgedeckt ist
	public static void printMineField(int[][][] spielfeld) {
		System.out.println(" X01234567");
		System.out.println("Y#########");
		for (int y = 0; y < 8; y++) {
			System.out.print(y+"#");
			for (int x = 0; x < 8; x++) {
				if (spielfeld[y][x][1] != 0) {
  				  	if (spielfeld[y][x][0] != -1) {
  				  		System.out.print(spielfeld[y][x][0]);
  				  	} else {
  				  		System.out.print('M');
  				  	}
				} else {
				  		System.out.print('*');
				}
			}
			System.out.println("");
		}
	}

	// Minen verteilen
	public static void allocateMines(int[][][] spielfeld) {
		Random myRnd = new Random();
		int verteilteMinen = 0;
		do {
			int x = myRnd.nextInt(8);
			int y = myRnd.nextInt(8);
			if (spielfeld[y][x][0] != -1) {
				spielfeld[y][x][0] = -1;
				verteilteMinen++;
			}
		} while (verteilteMinen != 10);
	}

	// Zahlen verteilen
	public static void allocateNumbers(int[][][] spielfeld) {
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if (spielfeld[y][x][0] != -1) {
					spielfeld[y][x][0] = countMines(spielfeld, y, x);
				}
			}
		}
	}

	// Anzahl der Minen in der Nachbarschaft ermitteln
	public static int countMines(int[][][] spielfeld, int x, int y) {
		int anzahlMinen = 0;
		for (int i = y-1; i <= y+1; i++) {
			for (int j = x-1; j <= x+1; j++) {
				// Nur umliegende Felder im Spielfeld beruecksichtigen (ohne das aktuelle Feld selbst)
				if (i >= 0 && i < 8 && j >= 0 && j < 8 && !( i == y && j == x)) {
					if (spielfeld[j][i][0] == -1) {
						anzahlMinen++;
					}
				}
			}
		}
		return anzahlMinen;
	}

	// Feld aufdecken
	public static boolean aufdecken(int[][][] spielfeld, int x, int y) {
		boolean erfolgreich = true;
		if (spielfeld[y][x][0] == -1) {
			erfolgreich = false;
		} else {
			spielfeld[y][x][1] = 1;
		}
		return erfolgreich;
	}

	// Ermitteln der aufgedeckten Felde
	public static int anzahlAufgedeckt(int[][][] spielfeld) {
		int zaehler = 0;
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if (spielfeld[y][x][1] != 0) {
					zaehler++;
				}
			}
		}
		return zaehler;
	}

	// Mainmethode
	public static void main(String[] args) {

		// Initialisierungen
		Scanner eingabe = new Scanner(System.in);
		String nochmal = "";

		// Spielablauf
		do {
			int[][][] spielfeld = new int[8][8][2];
			boolean erfolgreich;
			int aufgedeckt = 0;
			allocateMines(spielfeld);
			allocateNumbers(spielfeld);
			do {
				printMineField(spielfeld);
				System.out.print("X-Wert? ");
				int xtipp = eingabe.nextInt();
				System.out.print("Y-Wert? ");
				int ytipp = eingabe.nextInt();
				erfolgreich = aufdecken(spielfeld, xtipp, ytipp);
				aufgedeckt = anzahlAufgedeckt(spielfeld);
			} while (erfolgreich && aufgedeckt < 54);
            if (erfolgreich) {
            	System.out.println("Glueckwunsch! Sie haben alle Felder ohne Minen erfolgreich aufgedeckt!");
            } else {
            	System.out.println("Boooom! Sie sind auf eine Mine gestossen! Sie haben "+aufgedeckt+" Felder erfolgreich aufgedeckt!");
            }
			printFullMineField(spielfeld);
			System.out.println("Nochmal spielen (j/n) ? ");
			nochmal = eingabe.next();
		} while (nochmal.equals("j"));

	}

}
