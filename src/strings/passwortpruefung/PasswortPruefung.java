package strings.passwortpruefung;

/**
 * Programm zur Passwortpruefung.
 */
public class PasswortPruefung {

    /**
	 * Passwort darf nicht null sein und muss mindestens 6 Zeichen lang sein,
	 * mindestens einen Gross- und einen Kleinbuchstaben, sowie ein Sonderzeichen
	 * und eine Zahl enthalten.
	 * 
	 * @param pass Passwort
	 * @return true wenn Bedingungen erfuellt
	 */
    public boolean isPasswordOk(String pass) {
        if (pass == null || pass.length() < 6) {
            return false;
        }
        
        int grossbuchstabe = 0;
        int kleinbuchstabe = 0;
        int zahl = 0;
        int sonderzeichen = 0;
        
        for (int i = 0; i < pass.length(); i++) {
            char zeichen = pass.charAt(i);
            if (Character.isUpperCase(zeichen)) {
                grossbuchstabe++;
            } else if (Character.isLowerCase(zeichen)) {
                kleinbuchstabe++;
            } else if (Character.isDigit(zeichen)) {
                zahl++;
            } else {
                sonderzeichen++;
            }
        }
        return grossbuchstabe != 0 && kleinbuchstabe != 0 && zahl != 0 && sonderzeichen != 0;
    }

    public static void main(String[] args) {
        PasswortPruefung pwd = new PasswortPruefung();
        System.out.println("PWD passwort: " + pwd.isPasswordOk("passwort"));
        System.out.println("PWD Passwort: " + pwd.isPasswordOk("Passwort"));
        System.out.println("PWD Passw0rt: " + pwd.isPasswordOk("Passw0rt"));
        System.out.println("PWD Passw0r!: " + pwd.isPasswordOk("Passw0r!"));
        System.out.println("PWD Pa0!: " + pwd.isPasswordOk("Pa0!"));
    }

}
