package strings.checkmail;

public class Checkmail {

    public boolean isValidMailadress(String address) {
        int posAt = address.indexOf("@");
        int posLastDot = address.lastIndexOf(".");
        int laenge = address.length();
        // Kein AT oder kein Punkt
        if (posAt == -1 || posLastDot == -1) {
            return false;
        }
        // vor AT muss etwas stehen
        if (posAt < 1) {
            return false;
        }
        // Top Level Domain mindestens 2 Zeichen
        if (posLastDot > laenge - 3) {
            return false;
        }
        // Servername mindestens 1 Zeichen
        if (posAt > posLastDot - 2) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Checkmail checker = new Checkmail();

        String address = "Ungueltig!";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "no@dot";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "mail@server.de";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "mail.de";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "mail@.de";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "@server.de";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "mail@server.d";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "m@s.de";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));

        address = "someone@server.somewhere.com";
        System.out.println("Check: " + address + " Erg: " + checker.isValidMailadress(address));
    }
}