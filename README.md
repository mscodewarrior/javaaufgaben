# javaexamples
Java Aufgaben und Beispiele für Einsteiger

Dieses Repository enthält Aufgaben und Beispiele zu Java. Ich nutze diese u.a. in meinen Vorlesungen an der DHBW Stuttgart. Der Code hier ist public domain und kann von jedem gerne und nach Belieben für eigene Zwecke genutzt werden.

Falls jemand weitere Aufgaben und Beispiele hinzufügen möchte, Fehler findet oder sich sonst irgendwie beteiligen möchte freue ich mich. Eventuell kann man hier eine kleine Sammlung aufbauen die von Lehrenden und Lernenden genutzt werden kann.

Ich versuche die Aufgaben nach Themengebieten zu strukturieren und für jede Aufgabe mit Lösung ein eigenes Package anzulegen. In der Regel ist die Aufgabe als LibreOffice-Dokument und pdf-Dokument abgelegt und die zugehörige Klasse als .java-Datei mit gleichem Namen (es sei denn es handelt sich um mehrere Klassen).


